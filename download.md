## Current releases

### Osinfo database

The Osinfo database is updated frequently (whenever there is a relevant set
of new content) and so individual releases are not listed here. Please consult
the [download site](https://releases.pagure.org/libosinfo/) directly for
release archives, named with the filename pattern ``osinfodb-$DATE.tar.xz``.

### Osinfo database tools

#### [osinfo-db-tools 1.10.0](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.10.0.tar.xz) [(gpg)](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.10.0.tar.xz.asc) Feb 14th, 2022

Changes in this release include

* Port to libsoup3 (optional)
* Several CI improvements
* Several translation improvements

#### [libosinfo 1.10.0](https://releases.pagure.org/libosinfo/libosinfo-1.10.0.tar.xz) [(gpg)](https://releases.pagure.org/libosinfo/libosinfo-1.10.0.tar.xz.asc) Feb 14th, 2022

Changes in this release include:

* Add API for resolving multiple tree matches
* Add API for resolving multiple media matches
* Add API to match between two OsinfoTree
* Add API to match between two OsinfoMedia
* Add API to get a complete list of firmwares
* Add missing documentation of osinfo_os_add_firmware()
* Add release status to osinfo-query
* Add --all flag to all tools to report all matches
* Fix hiding database entries
* Adapt to libsoup3 which is now preferred over libsoup2
* Several CI improvements
* Several translations improvements

All source archives for the previous releases are available for download at
the [release hosting site](https://releases.pagure.org/libosinfo/)

Previous release notes are available at in the
[NEWS file](https://gitlab.com/libosinfo/libosinfo/blob/master/NEWS).

### Deployment procedure

#### osinfo-db-tools

The osinfo-db-tools package is a pre-requisite of osinfo-db and libosinfo,
so should be built and installed first. It provides a small set of tools
for managing the installation, bundling and distribution of database files

```
$ tar zxvf osinfo-db-tools-$VERSION.tar.gz
$ cd osinfo-db-tools-$VERSION
$ meson setup build -Dprefix=/usr -Dsysconfdir=/etc -Dlocalstatedir=/var
$ ninja -C build
$ sudo ninja -C build install
```

#### osinfo-db

The osinfo-db package does not contain any code, nor does it require building,
as it is purely XML database files. Its contents merely have to be unpacked in
the correct location, using the osinfo-db-import tool. Operating system vendors
packaging it, should use the --system argument to install in the primary
database location.

```
$ sudo osinfo-db-import --system osinfo-db-$VERSION.tar.xz
```

Local system administrators wishing to provide updated content for a host
without conflicting with the OS vendor supplied packages should use the
``--local`` argument to install it in ``/etc``

```
$ sudo osinfo-db-import --local osinfo-db-$VERSION.tar.xz
```

Unprivileged users wishing to provide their account with updated content
should use the ``--user`` argument to install it

```
$ osinfo-db-import --user osinfo-db-$VERSION.tar.xz
```

#### libosinfo

The libosinfo package provides the C library for querying the database, with
ability to use it from non-C languages via GObject Introspection

```
$ tar zxvf libosinfo-$VERSION.tar.gz
$ cd libosinfo-$VERSION
$ meson setup build -Dprefix=/usr -Dsysconfdir=/etc -Dlocalstatedir=/var
$ ninja -C build
$ sudo ninja -C build install
```