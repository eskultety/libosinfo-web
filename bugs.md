## Viewing reported bugs & features

The libosinfo project uses the [GitLab project](https://gitlab.com/groups/libosinfo/) issue tracker for receiving bug reports and feature requests.

 * [View libosinfo bugs](https://gitlab.com/libosinfo/libosinfo/-/issues)
 * [View osinfo-db-tools bugs](https://gitlab.com/libosinfo/osinfo-db-tools/-/issues)
 * [View osinfo-db bugs](https://gitlab.com/libosinfo/osinfo-db/-/issues)

## Reporting new bugs

Filing a new bug requires an account on [GitLab](https://gitlab.com/)

 * [File libosinfo bugs](https://gitlab.com/libosinfo/libosinfo/-/issues/new)
 * [File osinfo-db-tools bugs](https://gitlab.com/libosinfo/osinfo-db-tools/-/issues/new)
 * [File osinfo-db bugs](https://gitlab.com/libosinfo/osinfo-db/-/issues/new)

## OS distribution bug reporting

If you are using libosinfo packages provided by your OS distribution vendor,
it is preferable to report bugs in their own bug tracker first. If you are
able & willing to try compiling new versions of libosinfo, then by all means,
also report the bugs to the upstream bug tracker as described earlier
