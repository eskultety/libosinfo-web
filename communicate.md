## Mailing lists

The project no longer uses a mailing list for communication, however, the
historical archive of previous discussions is [still
available](https://www.redhat.com/mailman/listinfo/libosinfo).

Contributors are directed to the [GitLab Project](https://gitlab.com/libosinfo)
for code submissions, bug reports and feature suggestions.

## IRC channels

The libosinfo developers can often be found on the [OFTC IRC servers](https://oftc.net),
in the #virt channel, on on GNOME IRC servers, in the #boxes channel.
